async's auto for promises


```
#!javascript

var Promise = require('bluebird'),
	pAuto = require('promise-auto');

pAuto({
    get_data: function(){
        console.log('in get_data');
        // async code to get some data
        return Promise.resolve(['data', 'converted to array']);
    },
    make_folder: function(){
        console.log('in make_folder');
        // async code to create a directory to store a file in
        // this is run at the same time as getting the data
        return Promise.resolve('folder');
    },
    write_file: ['get_data', 'make_folder', function(results){
        console.log('in write_file', JSON.stringify(results));
        // once there is some data and the directory exists,
        // write the data to a file in the directory
        return Promise.resolve('filename');
    }],
    email_link: ['write_file', function(results){
        console.log('in email_link', JSON.stringify(results));
        // once the file is written let's email a link to it...
        // results.write_file contains the filename returned by write_file.
        return Promise.resolve({
        	file: results.write_file,
        	email: 'user@example.com'
        });
    }]
}).then(function(results) {
    console.log('results = ', results);
}).catch(function(err) {
	console.log('err = ', err);
})
```