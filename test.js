var assert = require('assert'),
	R = require('ramda'),
	Bluebird = require('bluebird'),
	pAuto = require('./lib');

describe('core', function() {

	describe('tasks', function() {

		it('supports promises as tasks', function(done) {
			pAuto({
				a: Bluebird.resolve(true),
				b: Bluebird.resolve(true)
			}).then(function(results) {
				assert(results.hasOwnProperty('a') && results.a === true);
				assert(results.hasOwnProperty('b') && results.b === true);
				done();
			}).catch(done);
		});

		it('supports functions that return promises as tasks', function(done) {
			pAuto({
				a: Bluebird.resolve(true),
				b: function() {
					return Bluebird.resolve(true);
				}
			}).then(function(results) {
				assert(results.hasOwnProperty('a') && results.a === true);
				assert(results.hasOwnProperty('b') && results.b === true);				
				done();
			}).catch(done);
		});

		it('supports dependencies', function(done) {
			pAuto({
				a: Bluebird.resolve(1),
				b: Bluebird.resolve(2),
				c: ['a', 'b', function(results) {
					return Bluebird.resolve(results.a + results.b);
				}]
			}).then(function(results) {
				assert(results.hasOwnProperty('a') && results.a === 1);
				assert(results.hasOwnProperty('b') && results.b === 2);
				assert(results.hasOwnProperty('c') && results.c === 3);
				done();
			}).catch(done);
		});

		it('allows you to define dependency-less tasks w/ & w/o wrapping it in an array', function(done) {
			pAuto({
				a: Bluebird.resolve('A'),
				b: [Bluebird.resolve('B')],
				c: ['a', 'b', Bluebird.resolve('C')],
				d: ['c', 'b', function(results) {
					assert(results.hasOwnProperty('a'));
					assert(results.hasOwnProperty('b'));
					assert(results.hasOwnProperty('c'));
					return Bluebird.resolve(results.c);
				}],
				z: function() {
					return Bluebird.resolve("Z");
				}
			}).then(function(results) {
				assert(results.hasOwnProperty('a') && results.a === 'A');
				assert(results.hasOwnProperty('b') && results.b === 'B');
				assert(results.hasOwnProperty('c') && results.c === 'C');
				assert(results.hasOwnProperty('d') && results.d === 'C');
				assert(results.hasOwnProperty('z') && results.z === 'Z');
				done();
			}).catch(done);
		});

		it('passes rejections to .catch', function(done) {
			pAuto({
				a: Bluebird.reject('foo')
			}).then(function() {
				done(new Error('Should not have called .then'));
			}).catch(function(err) {
				assert(err === 'foo');
				done();
			});
		});

	});

	describe('results', function() {
		var results;
		before(function(done) {
			pAuto({
				a: Bluebird.resolve('A'),
				b: Bluebird.resolve('B'),
				c: Bluebird.resolve('C'),
				d: ['a', 'b', 'c', Bluebird.resolve('D')],
				e: ['d', Bluebird.resolve('E')],
				z: Bluebird.resolve('Z') // not a dependency or a dependant
			}).then(function(_) {
				results = _;
				done();
			}).catch(done);
		});

		it('contains dependency task results', function() {
			assert(results.hasOwnProperty('a') && results.a === 'A');
			assert(results.hasOwnProperty('b') && results.b === 'B')
			assert(results.hasOwnProperty('c') && results.c === 'C')
		});	

		it('contains dependent task results', function() {
			assert(results.hasOwnProperty('d') && results.d === 'D');
			assert(results.hasOwnProperty('e') && results.e === 'E')
		});

		it('contains single nodes', function() {
			assert(results.hasOwnProperty('z') && results.z === 'Z');
		});
	});


});

describe('#spread', function() {
	it('passes arguments properly to the task handler', function(done) {
		pAuto.spread({
			a: Bluebird.resolve('A'),
			b: ['a', function(a) {
				assert(a === 'A');
				return Bluebird.resolve('B');
			}],
			c: ['a', 'b', function(a, b) {
				assert(a === 'A' && b === 'B');
				return Bluebird.resolve('C');
			}],
			d: ['b', 'a', function(b, a) {
				assert(b === 'B' && a === 'A')
				return Bluebird.resolve('D');
			}],
			e: ['d', function(d) {
				assert(d === 'D');
				return Bluebird.resolve('E');
			}]
		}).then(function() {
			done();
		}).catch(done);
	});
});

describe('#array', function() {
	it('passes arguments properly to the task handler', function(done) {
		pAuto.array({
			a: Bluebird.resolve('A'),
			b: ['a', function(arg) {
				assert(Array.isArray(arg));
				assert(arg.length === 1);
				assert(arg[0] === 'A');
				return Bluebird.resolve('B');
			}],
			C: ['a', 'b', function(arg) {
				assert(Array.isArray(arg));
				assert(arg.length === 2);
				assert(arg[0] === 'A');
				assert(arg[1] === 'B');
				return Bluebird.resolve('C');
			}],
			d: ['b', 'a', function(arg) {
				assert(Array.isArray(arg));
				assert(arg.length === 2);
				assert(arg[0] === 'B');
				assert(arg[1] === 'A');
				return Bluebird.resolve('D');
			}],
			E: ['d', function(arg) {
				assert(Array.isArray(arg));
				assert(arg.length === 1);
				assert(arg[0] === 'D');
				return Bluebird.resolve('E');
			}]
		}).then(function() {
			done();
		}).catch(done);
	});
});

describe('#object', function() {
	it('passes arguments properly to the task handler', function(done) {
		done();
	});
});