var R = require('ramda'),
	Bluebird = require('bluebird');


function isThenable(promise) {
	return promise !== null && promise === Object(promise) && typeof promise.then === 'function';
}
/*
var isThenable = R.allPass([
	R.not(R.eq(null)),
	R.converge(R.eq, R.identity, Object),
	R.compose(R.eq('function'), R.type, R.get('then'))
]);
*/

function getRunStack(tasks) {
	return Object.keys(tasks).map(function(taskName) {
		var task = tasks[taskName];
		if (!Array.isArray(task))
			task = [task];

		return {
			name: taskName,
			deps: task.slice(0, -1),
			promise: R.last(task)
		};	
	});
}

var getProp = R.flip(R.prop),
	hasProp = R.flip(R.has);

// task(dep1, dep2)
function argSpread(task, results) {
	//return task.promise.apply(task.promise, task.deps.map(getProp(results)));
	return R.apply(task.promise, R.map(getProp(results), task.deps));
}

// task({dep1, dep2})
function argObject(task, results) {
	return task.promise(R.pick(task.deps, results));
}

// task({dep1, dep2})
function argInstance(task, results) {
	return task.promise(results);
}

// task([dep1, dep2])
function argArray(task, results) {
	//return task.promise.call(task.promise, task.deps.map(getProp(results)));
	return R.call(task.promise, R.map(getProp(results), task.deps));
}

var auto = R.curry(function(caller, tasks) {
	var stack = getRunStack(tasks),
		tasksLeft = stack.length,
		results = {},
		isRejected = false;

	var isReady = R.compose(
		R.or(R.not(R.length), R.all(hasProp(results))),
		R.prop('deps')
	);

	return new Bluebird(function(resolve, reject) {
		var onComplete = R.curry(function(task, result) {
			results[task.name] = result;
			tasksLeft--;

			if (tasksLeft)
				update();
			else if (!isRejected)
				resolve(results);	
		});

		function onError(err) {
			if (!isRejected) reject(err);
			isRejected = true;
		}

		function update() {
			if (isRejected) return;

			stack = stack.filter(function(task) {
				if (!isReady(task)) return true;

				if (typeof task.promise === 'function') {
					caller(task,results)
						.then(onComplete(task))
						.catch(onError);
				} else if (isThenable(task.promise)) {
					task.promise
						.then(onComplete(task))
						.catch(onError);
				} else {
					onComplete(task, task.promise);
				}
				return false;	
			});
		}
		update();
	});
});

module.exports = auto(argInstance);
module.exports.spread = auto(argSpread);
module.exports.object = auto(argObject);
module.exports.array = auto(argArray);